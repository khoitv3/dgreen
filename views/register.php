<!DOCTYPE html>
<html lang="vi" data-sbro-popup-lock="true" data-sbro-deals-lock="true" data-sbro-ads-lock="true">
<head>
	<?php include 'style.php';?>
	<title>Đăng ký thành viên</title>
    <!-- Css files-->
    <link rel="stylesheet" type="text/css" href="css/styleCropImg.css" />
    <link rel="stylesheet" type="text/css" href="css/style-example.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.Jcrop.css" />
    <script type="text/javascript" src="js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="js/jquery.SimpleCropper.js"></script>
	<script lang="Javascript">
	    $(document).ready(function() {
	        $("#guiMa" ).click(function() {
	        	$("#ketQua").html("Thông tin đang được xác nhận. vui lòng chờ ít phút");
	        	$.post("../controller/confirmMail.php",
	   		    {
	   		    	mail: $("#email").val()
	   		    },
	   		    function(data,status){
	   		    	$("#ketQua").html(data);
	   		    });
	       	});
	    });
	</script>
</head>
<body class="scroll-run">   
	<div id="page" class="hfeed site">
		<?php include 'header.php';?>
		<div class="head-title">
			<div class="container">
				<div class="row"><h2 class="page-title">Đăng ký thành viên</h2></div>
			</div>
		</div>
		<div id="main">
			<div class="container">
				<div class="row">
					<div class="content-area col-md-8" id="primary">
						<div class="site-content" id="content">
							<article class="post hentry" style="min-height: 500px; padding-top: 15px;">
								<form action="../controller/doRegister.php" method="post">
<!-- 								<form action="../controller/demoUpload.php" method="post"> -->
									<div class="col-md-4">
										<div class="simple-cropper-images" align="center">
											<div class="cropme" style="width: 100%; height: 100%;">
												<img alt="" src="./images/img.png" style="position: relative; left: -10000px;">
											</div>
											<div class="clear"></div>
											<script>
												// Init Simple Cropper
												$('.cropme').simpleCropper();
											</script>
										</div>
									</div>
	      							<input type="hidden" id="dataUrl" name="images"/>
									<div class="col-md-8">
										<table class="table">
											<tr>
												<td style="width:100px; padding-top:15px;">Tài khoản</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
														<input type="text" name="account" class="form-control" placeholder="Tài khoản" aria-describedby="basic-addon1" />
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-top:15px;">Họ tên</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-check-square-o"></span></span>
														<input type="text" name="name" class="form-control" placeholder="Họ tên" aria-describedby="basic-addon1" />
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-top:15px;">Ngày sinh</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar-o"></span></span>
														<input type="text" name="dateOfBirth" class="form-control" placeholder="Ngày sinh" id="datepicker" aria-describedby="basic-addon1" />
													</div>
												</td>
											</tr>
											<tr>
												<td style="">Giới tính</td>
												<td>
													<input type="radio" name="gender" value="true" checked="" /> Nam 
													<input type="radio" name="gender" value="false" /> Nữ
												</td>
											</tr>
											<tr>
												<td style="padding-top:15px;">Điện thoại</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-tablet" style="margin: 0 2px;"></span></span>
														<input type="text" name="phoneNumber" class="form-control" placeholder="Số điện thoại" aria-describedby="basic-addon1" />
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-top:35px;">Email</td>
												<td>
								        			<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-envelope-o"></span></span>
														<input type="text" name="email" id="email" class="form-control" placeholder="Địa chỉ email" aria-describedby="basic-addon1" />
														<span class="input-group-addon btn btn-primary" style="cursor: pointer;" id="guiMa">Code</span>
													</div>
													<div class="input-group" style="margin-top: 5px;">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-unlock-alt" style="margin: 0 2px;"></span></span>
														<input type="password" name="confirm" class="form-control" placeholder="Mã xác nhận email" aria-describedby="basic-addon1" />
													</div>
													<span id="ketQua" style="font-size: small; color: red;"></span>
												</td>
											</tr>
											<tr>
												<td style="padding-top:15px;">Địa chỉ</td>
												<td>
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar-o"></span></span>
														<input type="text" name="addr" class="form-control" placeholder="Địa chỉ" aria-describedby="basic-addon1" />
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-top:15px; text-align: center;" colspan="2">
													<input class="btn btn-primary" type="submit" name="submit" value="Đăng ký" />
													<input class="btn btn-primary" type="reset" value="Làm mới" />
												</td>
											</tr>
										</table>
										<script src="js/bootstrap-datepicker.js"></script>
										<script type="text/javascript">
							                // When the document is ready
							                $(document).ready(function () {
							                    $('#datepicker').datepicker({
							                        format: "dd/mm/yyyy"
							                    });
							                });
							            </script>
									</div>
								</form>
							</article>
						</div>
					</div>
					<!-- Menu right -->
					<?php include 'right.php';?>
				</div>
			</div>
		</div>
		<?php include 'footer.php';?>
	</div>
</body>
</html>