<?php
class Comment extends DB_driver {
	private $idComment;
	private $account;
	private $idSeedling;
	private $substance;
	private $commentAt;
	private $updateAt;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdComment($value) {
		$this->idComment = $value;
	}
	public function getIdComment() {
		return $this->idComment;
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setIdSeedling($value) {
		$this->idSeedling = $value;
	}
	public function getIdSeedling() {
		return $this->idSeedling;
	}
	public function setSubstance($value) {
		$this->substance = $value;
	}
	public function getSubstance() {
		return $this->substance;
	}
	public function setCommentAt($value) {
		$this->commentAt = $value;
	}
	public function getCommentAt() {
		return $this->commentAt;
	}
	public function setUpdateAt($value) {
		$this->updateAt = $value;
	}
	public function getUpdateAt() {
		return $this->updateAt;
	}
	public function setData() {
		if ($this->idComment != null && $this->idComment != "") {
			$this->data ['IDComment'] = $this->idComment;
		}
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->idSeedling != null && $this->idSeedling != "") {
			$this->data ['IDSeedling'] = $this->idSeedling;
		}
		if ($this->substance != null && $this->substance != "") {
			$this->data ['Substance'] = $this->substance;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'CommentSeedling', $this->data );
	}
	function deleteCommentSeedling() {
		return $this->remove ( "CommentSeedling", "IDComment='" . $this->idComment . "'" );
	}
	function updateCommentSeedling() {
		$this->setData ();
		return $this->update ( "CommentSeedling", $this->data, "IDComment='" . $this->idComment . "'" );
	}
	function get($idComment) {
		$sql = "select * from CommentSeedling where IDComment = '" . idComment . "'";
		$row = $this->get_row ( $sql );
		$this->idComment = $row ['IDComment'];
		$this->account = $row ['Account'];
		$this->idSeedling = $row ['IDSeedling'];
		$this->substance = $row ['Substance'];
		$this->commentAt = $row ['CommentAt'];
		$this->updateAt = $row ['UpdateAt'];
	}
	public function getList($condition) {
		$sql = "select * from CommentSeedling";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$commentSeedling = new CommentSeedling ();
			$commentSeedling->idComment = $row ['IDComment'];
			$commentSeedling->account = $row ['Account'];
			$commentSeedling->idSeedling = $row ['IDSeedling'];
			$commentSeedling->substance = $row ['Substance'];
			$commentSeedling->commentAt = $row ['CommentAt'];
			$commentSeedling->updateAt = $row ['UpdateAt'];
			$return [] = $commentSeedling;
		}
		return $return;
	}
	public function __toString() {
		return $this->idComment . ' - ' . $this->account . ' - ' . $this->idSeedling . ' - ' . $this->substance . ' - ' . $this->commentAt . ' - ' . $this->updateAt;
	}
}