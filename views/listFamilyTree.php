<!DOCTYPE html>
<html lang="vi" data-sbro-popup-lock="true" data-sbro-deals-lock="true" data-sbro-ads-lock="true">
<head>
	<?php include 'style.php';?>
	<title>Danh sách Loại cây</title>
    <!-- Css files-->
    <link rel="stylesheet" type="text/css" href="css/styleCropImg.css" />
    <link rel="stylesheet" type="text/css" href="css/style-example.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.Jcrop.css" />
    <script type="text/javascript" src="js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="js/jquery.SimpleCropper.js"></script>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
</head>
<body class="scroll-run">   
	<div id="page" class="hfeed site">
		<?php include 'header.php';?>
		<div class="head-title">
			<div class="container">
				<div class="row"><h2 class="page-title">Danh sách Loại cây</h2></div>
			</div>
		</div>
		<div id="main">
			<div class="container">
				<div class="row">
					<div class="content-area col-md-8" id="primary">
						<div class="site-content" id="content">
							<article class="post hentry" style="min-height: 500px; padding-top: 15px;">
								<form method="post">
									<div class="col-md-12">					
										<table class="table table-hover">
											<thead>
												<tr>
											        <th>STT</th>
											        <th>Loại cây</th>
											        <th></th>
											    </tr>
											</thead>
											<tbody>
											    <tr>
											        <td>John</td>
											        <td>Doe</td>
											        <td>
											        	<button style="background-color: #5bc0de;border-color: #46b8da;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Sửa</button>
											        	<div class="modal fade" id="myModal" role="dialog">
															<div class="modal-dialog">
																<form action="../controller/doAddFamilyTree.php" method="post">
																<div style="margin: 20px;">
																	<table class="table">
																		<tr>
																			<td style="width:100px; padding-top:15px;">Loại cây</td>
																			<td>
																				<div class="input-group">
																					<span class="input-group-addon" id="basic-addon1"><span class="fa fa-tree"></span></span>
																					<input type="text" name="nameFamilyTree" class="form-control" placeholder="Loại cây" aria-describedby="basic-addon1" />
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td style="padding-top:15px;" colspan="2">Mô tả</td>
																		</tr>
																		<tr>
																			<td colspan="2" style="border-top:none;">
																				<textarea name="describe" rows="10" cols="80" id="describe"></textarea>
																				<script type="text/javascript">CKEDITOR.replace('describe'); </script>
																			</td>
																		</tr>
																		<tr>
																			<td style="padding-top:15px; text-align: center;" colspan="2">
																				<input class="btn btn-primary" type="submit" name="submit" value="Lưu" />
																				<input class="btn btn-primary" type="reset" value="Làm mới" />
																			</td>
																		</tr>
																	</table>
																</div>
															</form>
															</div>
														</div>
											        	<input class="btn btn-primary" type="submit" name="submit" value="Xóa" />
											        </td>
											     </tr>
												 <tr>
											        <td>Mary</td>
											        <td>Moe</td>
											        <td>
											        	<input style="background-color: #5bc0de;border-color: #46b8da;" class="btn btn-primary" type="submit" name="submit" value="Sửa" />
											        	<input class="btn btn-primary" type="submit" name="submit" value="Xóa" />
											        </td>
											     </tr>
											     <tr>
											        <td>July</td>
											        <td>Dooley</td>
											        <td>
											        	<input style="background-color: #5bc0de;border-color: #46b8da;" class="btn btn-primary" type="submit" name="submit" value="Sửa" />
											        	<input class="btn btn-primary" type="submit" name="submit" value="Xóa" />
											        </td>
											     </tr>
											   </tbody>
<!-- 											<tr> -->
												<td style="padding-top:15px; text-align: center;" colspan="2">
<!-- 													<input class="btn btn-primary" type="submit" name="submit" value="Đăng ký" /> -->
<!-- 													<input class="btn btn-primary" type="reset" value="Làm mới" /> -->
<!-- 												</td> -->
<!-- 											</tr> -->
										</table>
									</div>
								</form>
							</article>
						</div>
					</div>
					<!-- Menu right -->
					<?php include 'right.php';?>
				</div>
			</div>
		</div>
		<?php include 'footer.php';?>
	</div>
</body>
</html>