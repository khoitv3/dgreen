
<div class="head-title">
	<div class="container">
		<div class="row">
			<h2 class="page-title">Liên hệ</h2>
		</div>
	</div>
</div>
<div id="main">
	<div class="container">
		<div class="row">
			<div class="content-area col-md-8" id="primary">
				<div class="site-content" id="content">
					<article class="post hentry">
						<header class="entry-header">
							<div class="entry-map">
								<div class="map">
									<iframe
										src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2914.11494977817!2d-89.38065090636364!3d43.081078627494435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8806536cec95e4cf%3A0x5d9565d55f8647a0!2sGoogle+Madison!5e0!3m2!1sen!2s!4v1462712287520"
										width="600" height="450" frameborder="0" style="border: 0"
										allowfullscreen> </iframe>
								</div>
							</div>
							<h1 class="entry-title">
								<a></a>
							</h1>
						</header>
						<div class="entry-content">
							<p></p>
							<p>&nbsp;</p>
							<div class="row">
								<div class="col-md-6">
									<p>
										<strong>Trụ sở chính:</strong><br> Tòa nhà Hanoi Group - 442
										Đội Cấn - Ba Đình - Hà Nội<br> Điện thoại : (04) 6674 2332<br>
										Email : support@bizweb.vn

									</p>
								</div>
								<div class="col-md-6">
									<p>
										<strong>Văn phòng:</strong><br> 266 Đội Cấn - Ba Đình - Hà Nội<br>
										Điện thoại : (04) 6674 2332<br> Email : support@bizweb.vn

									</p>
								</div>
							</div>
						</div>
					</article>
					<div class="comment-outer">
						<div id="respond" class="comment-respond">
							<h2 id="reply-title" class="comment-reply-title">Liên hệ</h2>
							<form accept-charset="UTF-8"
								action="https://batakoo-theme.bizwebvietnam.net/contact"
								id="contact" method="post">
								<input name="FormType" type="hidden" value="contact"> <input
									name="utf8" type="hidden" value="true">

								<p class="comment-form-email">
									<label for="author">Họ tên</label> <span class="required">*</span>
									<input id="author" type="text" class="input-text"
										name="contact[name]">
								</p>
								<p class="comment-form-author">
									<label for="email">Email</label> <span class="required">*</span>
									<input id="email" type="text" class="input-text"
										name="contact[email]">
								</p>
								<p class="comment-form-comment">
									<label for="message">Nội dung</label>
									<textarea name="contact[body]" id="message" cols="45" rows="10"
										class="input-text"></textarea>
								</p>
								<p class="form-submit">
									<input class="btn btn-md btn-default" name="submit"
										type="submit" id="button" value="Gửi liên hệ">
								</p>
							</form>

						</div>
					</div>
				</div>
			</div>
			<!-- Menu right -->
			<?php include 'right.php';?>
		</div>
	</div>
</div>