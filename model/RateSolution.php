<?php
class RateSolution extends DB_driver {
	private $idSolution;
	private $account;
	private $numberStars;
	private $substance;
	private $rateAt;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIDSolution($value) {
		$this->iDSolution = $value;
	}
	public function getIDSolution() {
		return $this->idSolution;
	}
	public function setNumberStars($value) {
		$this->numberStars = $value;
	}
	public function getNumberStars() {
		return $this->numberStars;
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setSubstance($value) {
		$this->substance = $value;
	}
	public function getSubstance() {
		return $this->substance;
	}
	public function setRateAt($value) {
		$this->rateAt = $value;
	}
	public function getRateAt() {
		return $this->rateAt;
	}
	public function setData() {
		if ($this->idSolution != null && $this->idSolution != "") {
			$this->data ['IDSolution'] = $this->idSolution;
		}
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->numberStars != null && $this->numberStars != "") {
			$this->data ['NumberStars'] = $this->numberStars;
		}
		if ($this->substance != null && $this->substance != "") {
			$this->data ['Substance'] = $this->substance;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'RateSolution', $this->data );
	}
	function deleteRateSolution() {
		return $this->remove ( "RateSolution", "IDSolution='" . $this->idSolution . "'" );
	}
	function updateRateSolution() {
		$this->setData ();
		return $this->update ( "RateSolution", $this->data, "IDSolution='" . $this->idSolution . "'" );
	}
	function get($idSolution) {
		$sql = "select * from RateSolution where IDSolution = '" . $idSolution . "'";
		$row = $this->get_row ( $sql );
		$this->idSolution = $row ['IDSolution'];
		$this->account = $row ['Account'];
		$this->numberStars = $row ['NumberStars'];
		$this->substance = $row ['Substance'];
		$this->rateAt = $row ['RateAt'];
	}
	public function getList($condition) {
		$sql = "select * from RateSolution";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$rateSolution = new RateSolution ();
			$rateSolution->idSolution = $row ['IDSolution'];
			$rateSolution->account = $row ['Account'];
			$rateSolution->numberStars = $row ['NumberStars'];
			$rateSolution->substance = $row ['Substance'];
			$rateSolution->rateAt = $row ['RateAt'];
			$return [] = $rateSolution;
		}
		return $return;
	}
	public function __toString() {
		return $this->idSolution . ' - ' . $this->account . ' - ' . $this->numberStars . ' - ' . $this->substance . ' - ' . $this->rateAt;
	}
}