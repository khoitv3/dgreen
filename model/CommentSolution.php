<?php
class CommentSolution extends DB_driver {
	private $idComment;
	private $account;
	private $idSolution;
	private $substance;
	private $commentAt;
	private $updateAt;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdComment($value) {
		$this->idComment = $value;
	}
	public function getIdComment() {
		return $this->idComment;
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setIdSolution($value) {
		$this->idSolution = $value;
	}
	public function getIdSolution() {
		return $this->idSolution;
	}
	public function setSubstance($value) {
		$this->substance = $value;
	}
	public function getSubstance() {
		return $this->substance;
	}
	public function setCommentAt($value) {
		$this->commentAt = $value;
	}
	public function getCommentAt() {
		return $this->commentAt;
	}
	public function setUpdateAt($value) {
		$this->updateAt = $value;
	}
	public function getUpdateAt() {
		return $this->updateAt;
	}
	public function setData() {
		if ($this->idComment != null && $this->idComment != "") {
			$this->data ['IDComment'] = $this->idComment;
		}
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->idSolution != null && $this->idSolution != "") {
			$this->data ['IDSolution'] = $this->idSolution;
		}
		if ($this->substance != null && $this->substance != "") {
			$this->data ['Substance'] = $this->substance;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'CommentSolution', $this->data );
	}
	function deleteCommentSolution() {
		return $this->remove ( "CommentSolution", "IDComment='" . $this->idComment . "'" );
	}
	function updateCommentSolution() {
		$this->setData ();
		return $this->update ( "CommentSolution", $this->data, "IDComment='" . $this->idComment . "'" );
	}
	function get($idComment) {
		$sql = "select * from CommentSolution where IDComment = '" . $idComment . "'";
		$row = $this->get_row ( $sql );
		$this->idComment = $row ['IDComment'];
		$this->account = $row ['Account'];
		$this->idSolution = $row ['IDSolution'];
		$this->substance = $row ['Substance'];
		$this->commentAt = $row ['CommentAt'];
		$this->updateAt = $row ['UpdateAt'];
	}
	public function getList($condition) {
		$sql = "select * from CommentSolution";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$commentSolution = new CommentSolution ();
			$commentSolution->idComment = $row ['IDComment'];
			$commentSolution->account = $row ['Account'];
			$commentSolution->idSolution = $row ['IDSolution'];
			$commentSolution->substance = $row ['Substance'];
			$commentSolution->commentAt = $row ['CommentAt'];
			$commentSolution->updateAt = $row ['UpdateAt'];
			$return [] = $commentSolution;
		}
		return $return;
	}
	public function __toString() {
		return $this->idComment . ' - ' . $this->account . ' - ' . $this->idSolution . ' - ' . $this->substance . ' - ' . $this->commentAt . ' - ' . $this->updateAt;
	}
}