<?php
class FamilyTree extends DB_driver {
	private $idFamilyTree;
	private $nameFamilyTree;
	private $describe;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdFamilyTree($value) {
		$this->idFamilyTree = $value;
	}
	public function getIdFamilyTree() {
		return $this->idFamilyTree;
	}
	public function setNameFamilyTree($value) {
		$this->nameFamilyTree = $value;
	}
	public function getNameFamilyTree() {
		return $this->nameFamilyTree;
	}
	public function setDescribe($value) {
		$this->describe = $value;
	}
	public function getDescribe() {
		return $this->describe;
	}
	public function setData() {
		if ($this->nameFamilyTree != null && $this->nameFamilyTree != "") {
			$this->data ['NameFamilyTree'] = $this->nameFamilyTree;
		}
		if ($this->describe != null && $this->describe != "") {
			$this->data ['Describes'] = $this->describe;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'FamilyTree', $this->data );
	}
	function deleteFamilyTree() {
		return $this->remove ( "FamilyTree", "IDFamilyTree='" . $this->idFamilyTree . "'" );
	}
	function updateByAccount() {
		$this->setData ();
		return $this->update ( "FamilyTree", $this->data, "IDFamilyTree='" . $this->idFamilyTree . "'" );
	}
	function get($idFamilyTree) {
		$sql = "select * from FamilyTree where IDFamilyTree = '" . $idFamilyTree . "'";
		$row = $this->get_row ( $sql );
		$this->idFamilyTree = $row ['IDFamilyTree'];
		$this->nameFamilyTree = $row ['NameFamilyTree'];
		$this->describe = $row ['Describes'];
	}
	public function getList($condition) {
		$sql = "select * from FamilyTree";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$familyTree = new FamilyTree ();
			$familyTree->idFamilyTree = $row ['IDFamilyTree'];
			$familyTree->nameFamilyTree = $row ['NameFamilyTree'];
			$familyTree->describe = $row ['Describes'];
			$return [] = $familyTree;
		}
		return $return;
	}
	public function __toString() {
		return $this->idFamilyTree . ' - ' . $this->nameFamilyTree . ' - ' . $this->describe;
	}
}