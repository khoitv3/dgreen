<!DOCTYPE html>
<html>
<body>
<script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="../js/jquery.ajaxfileupload.js"></script>
<script lang="Javascript">
	function changeAnh(){
		$.post("../controller/demoFile.php",{ },
	    function(data,status){
			alert(data);
			$("#anhCaNhan").html("<img src='../seedling/"+data+"' class='img-thumbnail' width='100%'/>");
			$('#fileToUpload').val(data);
	    });
	}
    $(document).ready(function() {
        $('input[type=file]').ajaxfileupload({
            'action' : '../controller/upload.php',
            'onComplete' : function(response) {
//                 $('#upload').hide();
                var statusVal = JSON.stringify(response.status);
                if(statusVal == "false") {
                    $("#anhCaNhan").html("<font color='red'>"+ JSON.stringify(response.message) +" </font>");
                }   
                if(statusVal == "true") {
                	changeAnh();
                }           
            },
            'onStart' : function() {
                $('#upload').show();
            }
        });
    });
</script>
<form action="./testFile.php" method="post">
    Select image to upload:
    <input type="hidden" name="nameUpload" id="fileToUpload">
    <div id="anhCaNhan" style="width:500px; height: 500px; cursor: pointer;" onclick="$('input[type=file]').click()">
        <img src="../images/addImage.jpg" class="img-thumbnail" width="100%" />
    </div>
    <input type="file" name="fileToUpload" style="display:none;" accept="image/*"/>
    <input type="submit" value="Upload Image" name="submit">
</form>
<?php 
	session_start();
	session_destroy();
?>
</body>
</html>