<?php
require '../model/DB_driver.php';
require '../model/Solution.php';
$solution = new Solution();
session_start();
if(isset($_POST['submit'])) {
	$solution->setNameSolution($_POST['nameSolution']);
	$solution->setDescribe($_POST['describe']);
	$uploads_dir = '../solution/';
	$img = $_POST['images'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = uniqid() . '.png';
	$success = file_put_contents($uploads_dir.$file, $data);
	$solution->setImages($file);
	if($solution->addNew()) {
		echo "<font style='color:blue'>Thêm giải pháp thành công!</font>";
	}
}  else {
	header('Location: ../views/addSolution.php');
}