
<header id="masthead" class="site-header navbar-fixed-top">
	<div class="header-navigation">
		<div class="container">
			<div class="row">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".site-navigation"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div class="logo navbar-brand">
					<a href="https://batakoo-theme.bizwebvietnam.net/"> <img
						src="./images/header-logo.png" alt="Batakoo">
					</a>
				</div>
				<nav id="primary-navigation"
					class="site-navigation navbar-collapse collapse" role="navigation">
					<div class="nav-menu">
						<ul class="menu">


							<li class=""><a href="https://batakoo-theme.bizwebvietnam.net/">Trang
									chủ</a></li>



							<li class=""><a
								href="https://batakoo-theme.bizwebvietnam.net/gioi-thieu">Giới
									thiệu</a></li>



							<li class=""><a
								href="https://batakoo-theme.bizwebvietnam.net/tin-tuc">Tin tức</a></li>



							<li class=" active "><a
								href="https://batakoo-theme.bizwebvietnam.net/lien-he">Liên hệ</a></li>


						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>