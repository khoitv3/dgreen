<?php
require '../model/DB_driver.php';
require '../model/FamilyTree.php';
$familyTree = new FamilyTree();
session_start();
if(isset($_POST['submit'])) {
	$familyTree->setNameFamilyTree($_POST['nameFamilyTree']);
	$familyTree->setDescribe($_POST['describe']);
	$uploads_dir = '../familytree/';
	if($familyTree->addNew()) {
		echo "<font style='color:blue'>Thêm cây mới thành công!</font>";
	}else 
		header('Location: ../views/addFamilyTree.php');
}