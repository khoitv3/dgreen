<?php
require ('DB_driver.php');
	class Solution extends DB_driver {
		private $idSolution;
		private $nameSolution;
		private $describe;
		private $viewCount;
		private $createAt;
		private $updateAt;
		private $images;
		
		public function __construct(){
			parent::connect ();
		}
		function __destruct() {
			parent::dis_connect ();
		}
		public function setIdSolution($value){
			$this-> idSolution = $value;
		}
		
		public function getIdSolution(){
			return $this-> idSolution;
		}
		
		public function setNameSolution($value){
			$this-> nameSolution = $value;
		}
		
		public function getNameSolution(){
			return $this-> nameSolution;
		}
		
		public function setDescribe($value){
			$this-> describe = $value;
		}
		
		public function getDescribe(){
			return $this-> describe;
		}
		
		public function setViewCount($value){
			$this-> viewCount = $value;
		}
		
		public function getViewCount(){
			return $this-> viewCount;
		}
		
		public function setCreateAt($value){
			$this-> createAt = $value;
		}
		
		public function getCreateAt(){
			return $this-> createAt;
		}
		
		public function setUpdateAt($value){
			$this-> updateAt = $value;
		}
		
		public function getUpdateAt(){
			return $this-> updateAt;
		}
		
		public function setImages($value){
			$this-> images = $value;
		}
		
		public function getImages(){
			return $this-> images;
		}
		public function setData() {
			if($this->nameSolution != null && $this->nameSolution != "") {
				$this->data['NameSolution'] = $this->nameSolution;
			}
			if($this->describe != null && $this->describe != "") {
				$this->data['Describe'] = $this->describe;
			}
			if($this->viewCount != null && $this->viewCount != "") {
				$this->data['ViewCount'] = $this->viewCount;
			}
			if($this->images != null && $this->images != "") {
				$this->data['Images'] = $this->images;
			}
		}
		function addNew() {
			$this->setData ();
			return parent::insert ( 'Solution', $this->data );
		}
		function deleteSolution() {
			return $this->remove ( "Solution", "IDSolution='" . $this->idSolution."'" );
		}
		function updateSolution() {
			$this->setData ();
			return $this->update ( "Solution", $this->data, "IDSolution='" . $this->idSolution."'");
		}
		function get($idSolution) {
			$sql = "select * from Solution where IDSolution = '" . $idSolution . "'";
			$row = $this->get_row ( $sql );
			$this->idSolution = $row ['IDSolution'];
			$this->nameSolution = $row ['NameSolution'];
			$this->describe = $row ['Describe'];
			$this->viewCount = $row ['ViewCount'];
			$this->createAt = $row ['CreateAt'];
			$this->updateAt = $row ['UpdateAt'];
			$this->images = $row ['Addr'];
			$this->pass = $row ['Images'];
		}
		public function getList($condition) {
			$sql = "select * from Solution";
			if($condition != null && $condition != '') {
				$sql = $sql.' where '.$condition;
			}
			$result = $this->get_list ( $sql );
			$return = array ();
			foreach ( $result as $row ) {
				$solution = new Solution();
				$solution->idSolution = $row ['IDSolution'];
				$solution->nameSolution = $row ['NameSolution'];
				$solution->describe = $row ['Describe'];
				$solution->viewCount = $row ['ViewCount'];
				$solution->createAt = $row ['CreateAt'];
				$solution->updateAt = $row ['UpdateAt'];
				$solution->images = $row ['Images'];
				$return [] = $solution;
			}
			return $return;
		}
		public function __toString() {
			return $this->idSolution.' - '.
					$this->nameSolution.' - '.
					$this->describe.' - '.
					$this->viewCount.' - '.
					$this->createAt.' - '.
					$this->updateAt.' - '.
					$this->images;
		}
	}