create table Member(
	Account varchar(30) primary key,
	Name varchar(100) not null,
	DateOfBirth date not null,
	Gender bit not null,
	PhoneNumber varchar(20),
	Email varchar(40),
	Addr varchar(50),
	Pass varchar(20),
	CreateAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	Images varchar(2000),
	Auth varchar(10)
);
create table FamilyTree(
	IDFamilyTree int UNSIGNED AUTO_INCREMENT primary key,
	NameFamilyTree varchar(50) not null,
	Describes text
);

create table Seedling(
	IDSeedling varchar(10) primary key default '0',
	NameSeedling varchar(100) not null,
	Describes text,
	IDFamilyTree int UNSIGNED,
	Price decimal,
	ViewCount int default 0,
	CreateAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	Images varchar(2000),
    foreign key (IDFamilyTree) references FamilyTree(IDFamilyTree)
);
create table Solution(
	IDSolution varchar(10) primary key default '0',
	NameSolution varchar(2000) not null,
	Describes text,
	ViewCount int default 0,
	CreateAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	Images varchar(2000)
);
create table Bill(
	IDBill varchar(10) primary key default '0',
	Account varchar(30),
	DateOrder datetime,
	CreateAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	MoneyTotal decimal default 0,
	foreign key (Account) references Member(Account)
);
create table BillDetail(
	IDBill varchar(10),
	IDSeedling varchar(10),
	Quantum int,
	Price decimal,
	foreign key (IDBill) references Bill(IDBill),
	foreign key (IDSeedling) references Seedling(IDSeedling),
	primary key (IDBill,IDSeedling)
);
create table RateSeedling(
	IDSeedling varchar(10),
	Account varchar(30),
	NumberStars int,
	Substance text,
	RateAt datetime default CURRENT_TIMESTAMP,
	foreign key (IDSeedling) references Seedling(IDSeedling),
	foreign key (Account) references Member(Account),
	primary key(IDSeedling,Account)
);
create table RateSolution(
	IDSolution varchar(10),
	Account varchar(30),
	NumberStars int,
	Substance text,
	RateAt datetime default CURRENT_TIMESTAMP,
	foreign key (IDSolution) references Solution(IDSolution),
	foreign key (Account) references Member(Account),
	primary key(IDSolution,Account)
);
create table CommentSeedling(
	IDComment int UNSIGNED AUTO_INCREMENT primary key,
	Account varchar(30) not null,
	IDSeedling varchar(10) not null,
	Substance text not null,
	CommentAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	foreign key (Account) references Member(Account),
	foreign key (IDSeedling) references Seedling(IDSeedling)
);
create table SubCommentSeedling(
	IDSubComment int UNSIGNED AUTO_INCREMENT primary key,
	IDComment int UNSIGNED,
	Account varchar(30),
	Substance text not null,
	CommentAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	foreign key (IDComment) references CommentSeedling(IDComment),
	foreign key (Account) references Member(Account)
);
create table CommentSolution(
	IDComment int UNSIGNED AUTO_INCREMENT primary key,
	Account varchar(30) not null,
	IDSolution varchar(10) not null,
	Substance text not null,
	CommentAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	foreign key (Account) references Member(Account),
	foreign key (IDSolution) references Solution(IDSolution)
);
create table SubCommentSolution(
	IDSubComment int UNSIGNED AUTO_INCREMENT primary key,
	IDComment int UNSIGNED,
	Account varchar(30),
	Substance text not null,
	CommentAt datetime default CURRENT_TIMESTAMP,
	UpdateAt datetime on update CURRENT_TIMESTAMP,
	foreign key (IDComment) references CommentSolution(IDComment),
	foreign key (Account) references Member(Account)
);

-- trigger auto increment primary key
create table Bill_seq (
  id int(11) NOT NULL AUTO_INCREMENT primary key
);
create table Solution_seq (
  id int(11) NOT NULL AUTO_INCREMENT primary key
);
create table Seedling_seq (
  id int(11) NOT NULL AUTO_INCREMENT primary key
);
--
-- triggers bill
--
drop trigger if exists tg_bill_insert;
drop trigger if exists tg_solution_insert;
drop trigger if exists tg_seedling_insert;
DELIMITER //
create trigger tg_bill_insert before insert on Bill
 for each row begin
  insert into Bill_seq values (NULL);
  set NEW.IDBill = concat('HD', LPAD(LAST_INSERT_ID(), 8, '0'));
end
//
DELIMITER ;

DELIMITER //
create trigger tg_solution_insert before insert on Solution
 for each row begin
  insert into Solution_seq values (NULL);
  set NEW.IDSolution = concat('GP', LPAD(LAST_INSERT_ID(), 8, '0'));
end
//
DELIMITER ;

DELIMITER //
create trigger tg_seedling_insert before insert on Seedling
 for each row begin
  insert into Seedling_seq values (NULL);
  set NEW.IDSeedling = concat('CA', LPAD(LAST_INSERT_ID(), 8, '0'));
end
//
DELIMITER ;

DELIMITER //
create trigger tg_billdetail_insert before insert on BillDetail
for each row begin
  declare vprice decimal;
  set vprice = (select Price from Seedling where IDSeedling = NEW.IDSeedling);
  set NEW.Price = vprice;
  update Bill set MoneyTotal = MoneyTotal + vprice * NEW.Quantum where IDBill = NEW.IDBill;
end
//
DELIMITER ;

DELIMITER //
create trigger tg_billdetail_update before update on BillDetail
for each row begin
  declare vprice decimal;
  set vprice = (select Price from Seedling where IDSeedling = NEW.IDSeedling);
  set NEW.Price = vprice;
  update Bill set MoneyTotal = MoneyTotal + vprice * (NEW.Quantum - OLD.Quantum) where IDBill = NEW.IDBill;
end
//
DELIMITER ;

DELIMITER //
create trigger tg_billdetail_delete after delete on BillDetail
for each row begin
  declare vcount int;
  update Bill set MoneyTotal = MoneyTotal - OLD.Price * OLD.Quantum where IDBill = OLD.IDBill;
  set vcount = (select COUNT(*) from BillDetail where IDBill = OLD.IDBill);
  if vcount = 0 then
	delete from Bill where IDBill = OLD.IDBill;
  end if;
end
//
DELIMITER ;


