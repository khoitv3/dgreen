<?php
require ('DB_driver.php');
	class SubCommentSolution extends DB_driver {
		private $idSubComment;
		private $idComment;
		private $account;
		private $substance;
		private $commentAt;
		private $updateAt;
		private $data;
		
		public function __construct(){
			parent::connect ();
		}
		function __destruct() {
			parent::dis_connect ();
		}
		public function setIdSubComment($value){
			$this-> idSubComment = $value;
		}
		
		public function getIdSubComment(){
			return $this-> idSubComment;
		}
		
		public function setIdComment($value){
			$this-> idComment = $value;
		}
		
		public function getIdComment(){
			return $this-> idComment;
		}
		
		public function setAccount($value){
			$this-> account = $value;
		}
		
		public function getAccount(){
			return $this-> account;
		}
		
		public function setSubstance($value){
			$this-> substance = $value;
		}
		
		public function getSubstance(){
			return $this-> substance;
		}
		
		public function setCommentAt($value){
			$this-> commentAt = $value;
		}
		
		public function getCommentAt(){
			return $this-> commentAt;
		}
		
		public function setUpdateAt($value){
			$this-> updateAt = $value;
		}
		
		public function getUpdateAt(){
			return $this-> updateAt;
		}
		public function setData() {
			if($this->idSubComment != null && $this->idSubComment != "") {
				$this->data['IDSubComment'] = $this->idSubComment;
			}
			if($this->idComment != null && $this->idComment != "") {
				$this->data['IDComment'] = $this->idComment;
			}
			if($this->account != null && $this->account != "") {
				$this->data['Account'] = $this->account;
			}
			if($this->substance != null && $this->substance != "") {
				$this->data['Substance'] = $this->substance;
			}
		}
		function addNew() {
			$this->setData ();
			return parent::insert ( 'SubCommentSolution', $this->data );
		}
		function deleteSubCommentSolution() {
			return $this->remove ( "SubCommentSolution", "IDSubComment='" . $this->idSubComment."'" );
		}
		function updateSubCommentSolution() {
			$this->setData ();
			return $this->update ( "SubCommentSolution", $this->data, "IDSubComment='" . $this->idSubComment."'");
		}
		function get($idSubComment) {
			$sql = "select * from SubCommentSolution where IDSubComment = '" . $idSubComment . "'";
			$row = $this->get_row ( $sql );
			$this->idSubComment = $row ['IDSubComment'];
			$this->idComment = $row ['IDComment'];
			$this->account = $row ['Account'];
			$this->substance = $row ['Substance'];
			$this->commentAt = $row ['CommentAt'];
			$this->updateAt = $row ['UpdateAt'];
		}
		public function getList($condition) {
			$sql = "select * from SubCommentSolution";
			if($condition != null && $condition != '') {
				$sql = $sql.' where '.$condition;
			}
			$result = $this->get_list ( $sql );
			$return = array ();
			foreach ( $result as $row ) {
				$subCommentSolution = new SubCommentSolution ();
				$subCommentSolution->idSubComment = $row ['IDSubComment'];
				$subCommentSolution->idComment = $row ['IDComment'];
				$subCommentSolution->account = $row ['Account'];
				$subCommentSolution->substance = $row ['Substance'];
				$subCommentSolution->commentAt = $row ['CommentAt'];
				$subCommentSolution->updateAt = $row ['UpdateAt'];
				$return [] = $subCommentSolution;
			}
			return $return;
		}
		public function __toString() {
			return $this->idSubComment.' - '.
					$this->idComment.' - '.
					$this->account.' - '.
					$this->substance.' - '.
					$this->commentAt.' - '.
					$this->updateAt;
		}
	}