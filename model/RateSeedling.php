<?php
class RateSeedling extends DB_driver {
	private $idSeedling;
	private $account;
	private $numberStars;
	private $substance;
	private $rateAt;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdSeedling($value) {
		$this->idSeedling = $value;
	}
	public function getIdSeedling() {
		return $this->idSeedling;
	}
	public function setNumberStars($value) {
		$this->numberStars = $value;
	}
	public function getNumberStars() {
		return $this->numberStars;
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setSubstance($value) {
		$this->substance = $value;
	}
	public function getSubstance() {
		return $this->substance;
	}
	public function setRateAt($value) {
		$this->rateAt = $value;
	}
	public function getRateAt() {
		return $this->rateAt;
	}
	public function setData() {
		if ($this->idSeedling != null && $this->idSeedling != "") {
			$this->data ['IDSeedling'] = $this->idSeedling;
		}
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->numberStars != null && $this->numberStars != "") {
			$this->data ['NumberStars'] = $this->numberStars;
		}
		if ($this->substance != null && $this->substance != "") {
			$this->data ['Substance'] = $this->substance;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'RateSeedling', $this->data );
	}
	function deleteRateSeedling() {
		return $this->remove ( "RateSeedling", "IDSeedling='" . $this->idSeedling . "'" );
	}
	function updateRateSeedling() {
		$this->setData ();
		return $this->update ( "RateSeedling", $this->data, "IDSeedling='" . $this->idSeedling . "'" );
	}
	function get($idSeedling) {
		$sql = "select * from RateSeedling where IDSeedling = '" . $idSeedling . "'";
		$row = $this->get_row ( $sql );
		$this->idSeedling = $row ['IDSeedling'];
		$this->account = $row ['Account'];
		$this->numberStars = $row ['NumberStars'];
		$this->substance = $row ['Substance'];
		$this->rateAt = $row ['RateAt'];
	}
	public function getList($condition) {
		$sql = "select * from RateSeedling";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$rateSeedling = new RateSeedling ();
			$rateSeedling->idSeedling = $row ['IDSeedling'];
			$rateSeedling->account = $row ['Account'];
			$rateSeedling->numberStars = $row ['NumberStars'];
			$rateSeedling->substance = $row ['Substance'];
			$rateSeedling->rateAt = $row ['RateAt'];
			$return [] = $rateSeedling;
		}
		return $return;
	}
	public function __toString() {
		return $this->idSeedling . ' - ' . $this->account . ' - ' . $this->numberStars . ' - ' . $this->substance . ' - ' . $this->rateAt;
	}
}