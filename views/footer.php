<footer id="footer-section" class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="widget">
					<div class="widget-inner">
						<h3 class="widget-title">
							<span class="wrap-1">Về </span><span class="wrap-2">chúng tôi </span>
						</h3>
						<p>
							<img src="./images/logo-light.png" alt="Batakoo">
						</p>
						<p>Thành lập từ năm 2008, Batakoo là một thương hiệu uy tín, một
							trong những công ty xây dựng đầu tiên có hệ thống quản lý chất
							lượng theo tiêu chuẩn ISO 9001:1994 (2000)</p>
						<p>
							<a href="#" class="more">Xem thêm</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="widget">
					<div class="widget-inner">
						<h3 class="widget-title">
							<span class="wrap-1">Địa </span><span class="wrap-2">chỉ </span>
						</h3>
						<table>
							<tbody>
								<tr>
									<td><strong>Địa chỉ</strong></td>
									<td>: 266 Đội Cấn - Ba Đình - Hà Nội</td>
								</tr>
								<tr>
									<td><strong>Số điện thoại</strong></td>
									<td>: (04) 6674 2332</td>
								</tr>
								<tr>
									<td><strong>Email</strong></td>
									<td>: <a href="mailto:support@bizweb.vn">support@bizweb.vn</a></td>
								</tr>
								<tr>
									<td><strong>Giờ mở cửa</strong></td>
									<td>: 08:00 AM ‐ 17:00 PM</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="widget">
					<div class="widget-inner">
						<h3 class="widget-title">
							<span class="wrap-1">Tin </span><span class="wrap-2">mới nhất </span>
						</h3>
						<div class="var-section">
							<ul>
								<li><a href="#">Ý tưởng hay cho thiết kế phòng khách</a></li>
								<li><a href="#">Bố trí phòng khách nhà ống mang lại tài lộc cho gia chủ</a></li>
								<li><a href="#">Các bước trang trí phòng khách nhà ống thu hút hơn</a></li>
								<li><a href="#">Ứng dụng gỗ trong nội thất hiện đại</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<center>
		Cung cấp bởi <a href="https://www.facebook.com/zMy.Stupidz" title="Trần Văn Khôi">Trần Văn Khôi</a>
	</center>
</footer>