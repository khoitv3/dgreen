<?php
class BillDetail extends DB_driver {
	private $idBill;
	private $idSeedling;
	private $quantum;
	private $price;
	private $data;
	public function __construct() {
	}
	public function setIdBill($value) {
		$this->idBill = $value;
	}
	public function getIdBill() {
		return $this->idBill;
	}
	public function setIdSeedling($value) {
		$this->idSeedling = $value;
	}
	public function getIdSeedling() {
		return $this->idSeedling;
	}
	public function setQuantum($value) {
		$this->quantum = $value;
	}
	public function getQuantum() {
		return $this->quantum;
	}
	public function setPrice($value) {
		$this->price = $value;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setData() {
		if ($this->idBill != null && $this->idBill != "") {
			$this->data ['IDBill'] = $this->idBill;
		}
		if ($this->idSeedling != null && $this->idSeedling != "") {
			$this->data ['IDSeedling'] = $this->idSeedling;
		}
		if ($this->quantum != null && $this->quantum != "") {
			$this->data ['Quantum'] = $this->quantum;
		}
		if ($this->price != null && $this->price != "") {
			$this->data ['Price'] = $this->price;
		}
	}
	public function addNew() {
		$this->setData ();
		return parent::insert ( 'BillDetail', $this->data );
	}
	public function deleteById() {
		return $this->remove ( 'BillDetail', "IDBill='" . $this->idBill . "' and IDSeedling = '" . $this->idSeedling . "'" );
	}
	public function updateById() {
		$this->setData ();
		return $this->update ( 'BillDetail', $this->data, "IDBill='" . $this->idBill . "' and IDSeedling = '" . $this->idSeedling . "'" );
	}
	public function get($idBill, $idSeedling) {
		$sql = "select * from BillDetail where IDBill='" . $idBill . "' and IDSeedling = '" . $idSeedling . "'";
		$row = $this->get_row ( $sql );
		$this->idBill = $row ['IDBill'];
		$this->idSeedling = $row ['IDSeedling'];
		$this->quantum = $row ['Quantum'];
		$this->price = $row ['Price'];
	}
	public function getByIdBill($idBill) {
		$sql = "select * from BillDetail where IDBill='" . $idBill . "'";
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$bill = new BillDetail();
			$bill->idBill = $row ['IDBill'];
			$bill->idSeedling = $row ['IDSeedling'];
			$bill->quantum = $row ['Quantum'];
			$bill->price = $row ['Price'];
			$return [] = $bill;
		}
		return $return;
	}
	public function getList($condition) {
		$sql = "select * from BillDetail";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$bill = new BillDetail();
			$bill->idBill = $row ['IDBill'];
			$bill->idSeedling = $row ['IDSeedling'];
			$bill->quantum = $row ['Quantum'];
			$bill->price = $row ['Price'];
			$return [] = $bill;
		}
		return $return;
	}
	public function __toString() {
		return $this->idBill . ' - ' . $this->idSeedling . ' - ' . $this->quantum . ' - ' . $this->price;
	}
}