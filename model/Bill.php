<?php
class Bill extends DB_driver {
	private $idBill;
	private $account;
	private $dateOrder;
	private $createAt;
	private $updateAt;
	private $moneyTotal;
	private $data;
	// Hàm Khởi Tạo
	function __construct() {
		parent::connect ();
	}
	// Hàm ngắt kết nối
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdBill($value) {
		$this->idBill = $value;
	}
	public function getIdBill() {
		return $this->idBill;
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setDateOrder($value) {
		$this->dateOrder = $value;
	}
	public function getDateOrder() {
		return $this->dateOrder;
	}
	public function getCreateAt() {
		return $this->createAt;
	}
	public function setUpdateAt($value) {
		$this->updateAt = $value;
	}
	public function getUpdateAt() {
		return $this->updateAt;
	}
	public function setImages($value) {
		$this->images = $value;
	}
	public function setMoneyTotal($value) {
		$this->moneyTotal = $value;
	}
	public function getMoneyTotal() {
		return $this->moneyTotal;
	}
	public function setData() {
		if ($this->idBill != null && $this->idBill != "") {
			$this->data ['IDBill'] = $this->idBill;
		}
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->dateOrder != null && $this->dateOrder != "") {
			$this->data ['DateOrder'] = $this->dateOrder;
		}
		if ($this->moneyTotal != null && $this->moneyTotal != "") {
			$this->data ['MoneyTotal'] = $this->moneyTotal;
		}
	}
	public function addNew() {
		$this->setData ();
		return parent::insert ( 'Bill', $this->data );
	}
	public function deleteById() {
		return $this->remove ( 'Bill', "IDBill='" . $this->idBill . "'" );
	}
	public function updateById() {
		$this->setData ();
		return $this->update ( 'Bill', $this->data, "IDBill='" . $this->idBill . "'" );
	}
	public function get($id) {
		$sql = "select * from Bill where IDBill = '" . $id . "'";
		$row = $this->get_row ( $sql );
		$this->idBill = $row ['IDBill'];
		$this->account = $row ['Account'];
		$this->dateOrder = $row ['DateOrder'];
		$this->createAt = $row ['CreateAt'];
		$this->updateAt = $row ['UpdateAt'];
		$this->moneyTotal = $row ['MoneyTotal'];
	}
	public function getList($condition) {
		$sql = "select * from Bill";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$bill = new Bill ();
			$bill->idBill = $row ['IDBill'];
			$bill->account = $row ['Account'];
			$bill->dateOrder = $row ['DateOrder'];
			$bill->createAt = $row ['CreateAt'];
			$bill->updateAt = $row ['UpdateAt'];
			$bill->moneyTotal = $row ['MoneyTotal'];
			$return [] = $bill;
		}
		return $return;
	}
	public function __toString() {
		return $this->idBill . ' - ' . $this->account . ' - ' . $this->dateOrder . ' - ' . $this->createAt . ' - ' . $this->updateAt . ' - ' . $this->moneyTotal;
	}
}