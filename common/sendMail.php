<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require "class.phpmailer.php";
require "class.smtp.php";
class SendMail {
	private $mail;
	private $subject;
	private $body;
	private $email;
	private $name;
	public function setSubject($value) {
		$this->subject = $value;
	}
	public function setBody($value) {
		$this->body;
	}
	public function setEmail($value) {
		$this->email = $value;
	}
	public function setName($value) {
		$this->name = $value;
	}
	public function __construct($email,$subject,$body,$name) {
		$this->mail = new PHPMailer();
		$this->mail->IsSMTP(); // set mailer to use SMTP
		$this->mail->Host = "smtp.gmail.com"; // specify main and backup server
		$this->mail->Port = 465; // set the port to use
		$this->mail->SMTPAuth = true; // turn on SMTP authentication
		$this->mail->SMTPSecure = 'ssl';
		$this->mail->Username = "dct.edu.2015@gmail.com"; // your SMTP username or your gmail username
		$this->mail->Password = "sieungoc"; // your SMTP password or your gmail password
		$from = "noreply@dgreen.com"; // Reply to this email
		$to=$email; // Recipients email ID
		$this->mail->From = $from;
		$this->mail->FromName="Dgreen"; // Name to indicate where the email came from when the recepient received
		if($name != null && $name != "") {
			$name = '=?utf-8?B?'.base64_encode($name).'?=';
		}
		$this->mail->AddAddress($to,$name);
		
		$this->mail->AddReplyTo($from,"Dgreen");
		$this->mail->WordWrap = 50; // set word wrap
		$this->mail->IsHTML(true); // send as HTML
		$this->subject = '=?utf-8?B?'.base64_encode($subject).'?=';
		$this->body = $body;
	}
	public function sendMail() {
		// $this->mail->ContentType = "text/html; charset=UTF-8";
		$this->mail->Subject = $this->subject;
		$this->mail->Body = $this->body; //HTML Body
		//$this->mail->SMTPDebug = 2;
		if($this->mail->Send()){
			return true;
		}
		return false;
	}
}
?>