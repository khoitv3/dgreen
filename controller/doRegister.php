<?php
require '../model/DB_driver.php';
require '../model/Member.php';
$member = new Member();
session_start();
if(isset($_POST['submit'])) {
	$email = $_POST['email'];
	$confirm = $_POST['confirm'];
	if(isset($_SESSION['mail']) && isset($_SESSION['code'])) {
		if($_SESSION['mail'] == $email && $_SESSION['code'] == $confirm) {
			$member->setEmail($email);
			unset($_SESSION['mail']);
			unset($_SESSION['code']);
		} else {
			unset($_SESSION['mail']);
			unset($_SESSION['code']);
			header('Location: ../views/register.php');
		}
	} else {
		header('Location: ../views/register.php');
	}
	$member->setAccount($_POST['account']);
	$member->setName($_POST['name']);
	
	$myDateTime = date("Y-m-d", strtotime($_POST['dateOfBirth']));
	$member->setDateOfBirth($myDateTime);
	$member->setGender($_POST['gender'] === 'true'? true: false);
	$member->setPhoneNumber($_POST['phoneNumber']);
	
	$member->setAddr($_POST['addr']);
	$member->setPass(rand ( 100000 , 999999));
	
	$uploads_dir = '../member/';
	$img = $_POST['images'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = uniqid() . '.png';
	$success = file_put_contents($uploads_dir.$file, $data);
	$member->setImages($file);
	if($member->addNew()) {
		require '../common/sendMail.php';
		$subject = "Thông tin tài khoản ".$member->getAccount();
		$body = "Xin chào {$member->getName()}. Bạn đã đăng ký thành công. mật khẩu đăng nhập của bạn là : {$member->getPass()}. <br/>Đăng nhập để biết thông tin chi tiết.";
		@$sendMail = new SendMail($email,$subject,$body,$member->getName());
		if(@$sendMail->sendMail()){
			echo "<font style='color:blue'>Đã gửi thành công. vui lòng kiểm tra lại email</font>";
		} else {
			echo "<font style='color:red'>Không thể kết nối đến email bạn cung cấp</font>";
		}
	}
}  else {
	header('Location: ../views/register.php');
}