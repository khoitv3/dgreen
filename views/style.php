<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="robots" content="noodp,index,follow">
<meta name="revisit-after" content="1 days">
<meta name="keywords" content="Batakoo,Themes,DKT,Bizweb">
<link rel="canonical"
	href="http://batakoo-theme.bizwebvietnam.net/lien-he">
<link rel="icon" href="images/logo.png">
<meta name="description" content="Liên hệ">

<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/datepicker.css" rel="stylesheet" type="text/css">

<script type="text/javascript" async src="./js/analytics.js"></script>
<script async src="./js/gtm.js"></script>
<script type="text/javascript" async src="./js/22409.js"></script>
<script type="text/javascript" async src="./js/bizweb_stats.js"></script>
<script src="./js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="./js/option-selectors.js" type="text/javascript"></script>
<script src="./js/api.jquery.js" type="text/javascript"></script>
<script src="./js/bootstrap.min.js" type="text/javascript"></script>
<script src="./js/plugin.js" type="text/javascript"></script>
<script src="./js/main.js" type="text/javascript"></script>
