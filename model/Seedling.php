<?php
class Seedling extends DB_driver {
	private $idSeedling;
	private $nameSeedling;
	private $describe;
	private $idFamilyTree;
	private $price;
	private $viewCount;
	private $createAt;
	private $updateAt;
	private $images;
	private $data;
	public function __construct() {
		parent::connect ();
	}
	function __destruct() {
		parent::dis_connect ();
	}
	public function setIdSeedling($value) {
		$this->idSeedling = $value;
	}
	public function getIdSeedling() {
		return $this->idSeedling;
	}
	public function setNameSeedling($value) {
		$this->nameSeedling = $value;
	}
	public function getNameSeedling() {
		return $this->nameSeedling;
	}
	public function setDescribe($value) {
		$this->describe = $value;
	}
	public function getDescribe() {
		return $this->describe;
	}
	public function setIdFamilyTree($value) {
		$this->idFamilyTree = $value;
	}
	public function getIdFamilyTree() {
		return $this->idFamilyTree;
	}
	public function setPrice($value) {
		$this->price = $value;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setViewCount($value) {
		$this->viewCount = $value;
	}
	public function getViewCount() {
		return $this->viewCount;
	}
	public function setCreateAt($value) {
		$this->createAt = $value;
	}
	public function getCreateAt() {
		return $this->createAt;
	}
	public function setUpdateAt($value) {
		$this->updateAt = $value;
	}
	public function getUpdateAt() {
		return $this->updateAt;
	}
	public function setImages($value) {
		$this->images = $value;
	}
	public function getImages() {
		return $this->images;
	}
	public function setData() {
		if ($this->idSeedling != null && $this->idSeedling != "") {
			$this->data ['IDSeedling'] = $this->idSeedling;
		}
		if ($this->nameSeedling != null && $this->nameSeedling != "") {
			$this->data ['NameSeedling'] = $this->nameSeedling;
		}
		if ($this->describe != null && $this->describe != "") {
			$this->data ['Describes'] = $this->describe;
		}
		if ($this->idFamilyTree != null && $this->idFamilyTree != "") {
			$this->data ['IDFamilyTree'] = $this->idFamilyTree;
		}
		if ($this->price != null && $this->price != "") {
			$this->data ['Price'] = $this->price;
		}
		if ($this->images != null && $this->images != "") {
			$this->data ['Images'] = $this->images;
		}
	}
	function addNew() {
		$this->setData ();
		return parent::insert ( 'Seedling', $this->data );
	}
	function deleteSeedling() {
		return $this->remove ( "Seedling", "IDSeedling='" . $this->idSeedling . "'" );
	}
	function updateSeedling() {
		$this->setData ();
		return $this->update ( "Seedling", $this->data, "IDSeedling='" . $this->idSeedling . "'" );
	}
	function get($idSeedling) {
		$sql = "select * from Seedling where IDSeedling = '" . $idSeedling . "'";
		$row = $this->get_row ( $sql );
		$this->idSeedling = $row ['IDSeedling'];
		$this->nameSeedling = $row ['NameSeedling'];
		$this->idFamilyTree = $row ['IDFamilyTree'];
		$this->price = $row ['Price'];
		$this->viewCount = $row ['ViewCount'];
		$this->createAt = $row ['CreateAt'];
		$this->updateAt = $row ['UpdateAt'];
		$this->images = $row ['Images'];
	}
	public function getList($condition) {
		$sql = "select * from Seedling";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$seedling = new Seedling ();
			$seedling->idSeedling = $row ['IDSeedling'];
			$seedling->nameSeedling = $row ['NameSeedling'];
			$seedling->describe = $row ['Describes'];
			$seedling->idFamilyTree = $row ['IDFamilyTree'];
			$seedling->price = $row ['Price'];
			$seedling->viewCount = $row ['ViewCount'];
			$seedling->createAt = $row ['CreateAt'];
			$seedling->updateAt = $row ['UpdateAt'];
			$seedling->images = $row ['Images'];
			$return [] = $seedling;
		}
		return $return;
	}
	public function __toString() {
		return $this->idSeedling . ' - ' . $this->nameSeedling . ' - ' . $this->describe . ' - ' . $this->idFamilyTree . ' - ' . $this->price . ' - ' . $this->viewCount . ' - ' . $this->createAt . ' - ' . $this->updateAt . ' - ' . $this->images;
	}
}