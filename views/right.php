
<aside id="secondary" class="col-md-4">
	<div class="sidebar">

		<div id="search-2" class="widget widget_search">
			<h2 class="widget-title">
				<span class="wrap-1">Tìm </span><span class="wrap-2">kiếm </span>
			</h2>
			<div class="searchform">
				<form action="https://batakoo-theme.bizwebvietnam.net/search">
					<input type="text" class="txt" name="query"
						placeholder="Nhập từ khóa tìm kiếm"> <input type="submit"
						value="search" class="btn btn-sm">
				</form>
			</div>
		</div>


		<div class="widget post-type-widget">
			<h2 class="widget-title">
				<span class="wrap-1">Bài </span><span class="wrap-2">viết mới nhất </span>
			</h2>
			<ul>

				<li><span class="post-category"> </span>
					<figure class="post-thumbnail">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/y-tuo-ng-hay-cho-thie-t-ke-pho-ng-kha-ch"><img
							alt="Ý tưởng hay cho thiết kế phòng khách"
							src="./images/1413hinh-nen-khong-gian-dep-truoc-canh-hoang-hon.jpg"></a>
					</figure>
					<h3 class="post-title">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/y-tuo-ng-hay-cho-thie-t-ke-pho-ng-kha-ch">Ý
							tưởng hay cho thiết kế phòng khách</a>
					</h3></li>

				<li><span class="post-category"> </span>
					<figure class="post-thumbnail">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/bo-tri-phong-khach-nha-ong-mang-lai-tai-loc-cho-gia-chu"><img
							alt="Bố trí phòng khách nhà ống mang lại tài lộc cho gia chủ"
							src="./images/ngu240314-12.jpg"></a>
					</figure>
					<h3 class="post-title">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/bo-tri-phong-khach-nha-ong-mang-lai-tai-loc-cho-gia-chu">Bố
							trí phòng khách nhà ống mang lại tài lộc cho gia chủ</a>
					</h3></li>

				<li><span class="post-category"> </span>
					<figure class="post-thumbnail">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/cac-buoc-trang-tri-de-phong-khach-nha-ong-tro-nen-thu-hut-hon"><img
							alt="Các bước trang trí phòng khách nhà ống thu hút hơn"
							src="./images/phong-khach-an-tuong-hon-voi-cach-tao-diem-nhan8.jpg"></a>
					</figure>
					<h3 class="post-title">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/cac-buoc-trang-tri-de-phong-khach-nha-ong-tro-nen-thu-hut-hon">Các
							bước trang trí phòng khách nhà ống thu hút hơn</a>
					</h3></li>

				<li><span class="post-category"> </span>
					<figure class="post-thumbnail">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/ung-dung-go-trong-noi-that-hien-dai"><img
							alt="Ứng dụng gỗ trong nội thất hiện đại"
							src="./images/cua-so-cho-khong-gian-noi-that-766.jpg"></a>
					</figure>
					<h3 class="post-title">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/ung-dung-go-trong-noi-that-hien-dai">Ứng
							dụng gỗ trong nội thất hiện đại</a>
					</h3></li>

				<li><span class="post-category"> </span>
					<figure class="post-thumbnail">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/3-ngoi-nha-tre-viet-nam-noi-tieng-the-gioi"><img
							alt="3 ngôi nhà tre Việt Nam nổi tiếng thế giới"
							src="./images/b-1-fc2b26c4-80f2-4afd-b3b4-f1a867d42f8e.jpg"></a>
					</figure>
					<h3 class="post-title">
						<a
							href="https://batakoo-theme.bizwebvietnam.net/3-ngoi-nha-tre-viet-nam-noi-tieng-the-gioi">3
							ngôi nhà tre Việt Nam nổi tiếng thế giới</a>
					</h3></li>

			</ul>
		</div>


		<div class="widget">
			<h2 class="widget-title">
				<span class="wrap-1">Danh </span><span class="wrap-2">mục tin tức </span>
			</h2>
			<ul>

				<li><a class="pull-left"
					href="https://batakoo-theme.bizwebvietnam.net/du-an-da-thuc-hien">Dự
						án đã thực hiện</a> <span class="pull-right">6</span></li>

				<li><a class="pull-left"
					href="https://batakoo-theme.bizwebvietnam.net/tin-tuc">Tin tức</a>
					<span class="pull-right">5</span></li>

			</ul>
		</div>


	</div>
</aside>