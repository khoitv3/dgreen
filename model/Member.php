<?php
class Member extends DB_driver {
	private $account;
	private $name;
	private $dateOfBirth;
	private $gender;
	private $phoneNumber;
	private $email;
	private $addr;
	private $pass;
	private $createAt;
	private $updateAt;
	private $images;
	private $auth;
	private $data;
	// Hàm Khởi Tạo
	function __construct() {
		parent::connect ();
	}
	// Hàm ngắt kết nối
	function __destruct() {
		parent::dis_connect ();
	}
	public function setAccount($value) {
		$this->account = $value;
	}
	public function getAccount() {
		return $this->account;
	}
	public function setName($value) {
		$this->name = $value;
	}
	public function getName() {
		return $this->name;
	}
	public function setDateOfBirth($value) {
		$this->dateOfBirth = $value;
	}
	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}
	public function setGender($value) {
		$this->gender = $value;
	}
	public function getGender() {
		return $this->gender;
	}
	public function setPhoneNumber($value) {
		$this->phoneNumber = $value;
	}
	public function getPhoneNumber() {
		return $this->phoneNumber;
	}
	public function setEmail($value) {
		$this->email = $value;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setAddr($value) {
		$this->addr = $value;
	}
	public function getAddr() {
		return $this->addr;
	}
	public function setPass($value) {
		$this->pass = $value;
	}
	public function getPass() {
		return $this->pass;
	}
	public function setCreateAt($value) {
		$this->createAt = $value;
	}
	public function getCreateAt() {
		return $this->createAt;
	}
	public function setUpdateAt($value) {
		$this->updateAt = $value;
	}
	public function getUpdateAt() {
		return $this->updateAt;
	}
	public function setImages($value) {
		$this->images = $value;
	}
	public function getImages() {
		return $this->images;
	}
	public function setAuth($value) {
		$this->auth = $value;
	}
	public function getAuth() {
		return $this->auth;
	}
	public function setData() {
		if ($this->account != null && $this->account != "") {
			$this->data ['Account'] = $this->account;
		}
		if ($this->name != null && $this->name != "") {
			$this->data ['Name'] = $this->name;
		}
		if ($this->dateOfBirth != null && $this->dateOfBirth != "") {
			$this->data ['DateOfBirth'] = $this->dateOfBirth;
		}
		if ($this->gender != null && $this->gender != "") {
			$this->data ['Gender'] = $this->gender;
		}
		if ($this->phoneNumber != null && $this->phoneNumber != "") {
			$this->data ['PhoneNumber'] = $this->phoneNumber;
		}
		if ($this->email != null && $this->email != "") {
			$this->data ['Email'] = $this->email;
		}
		if ($this->addr != null && $this->addr != "") {
			$this->data ['Addr'] = $this->addr;
		}
		if ($this->pass != null && $this->pass != "") {
			$this->data ['Pass'] = $this->pass;
		}
		if ($this->images != null && $this->images != "") {
			$this->data ['Images'] = $this->images;
		}
		if ($this->auth != null && $this->auth != "") {
			$this->data ['Auth'] = $this->auth;
		}
	}
	public function addNew() {
		$this->setData ();
		return parent::insert ( 'Member', $this->data );
	}
	public function deleteByAccount() {
		return $this->remove ( "Member", "Account='" . $this->account . "'" );
	}
	public function updateByAccount() {
		$this->setData ();
		return $this->update ( "Member", $this->data, "Account='" . $this->account . "'" );
	}
	public function get($account) {
		$sql = "select * from Member where Account = '" . $account . "'";
		$row = $this->get_row ( $sql );
		$this->account = $row ['Account'];
		$this->name = $row ['Name'];
		$this->dateOfBirth = $row ['DateOfBirth'];
		$this->gender = $row ['Gender'];
		$this->phoneNumber = $row ['PhoneNumber'];
		$this->email = $row ['Email'];
		$this->addr = $row ['Addr'];
		$this->pass = $row ['Pass'];
		$this->createAt = $row ['CreateAt'];
		$this->updateAt = $row ['UpdateAt'];
		$this->images = $row ['Images'];
		$this->auth = $row ['Auth'];
	}
	public function getList($condition) {
		$sql = "select * from Member";
		if ($condition != null && $condition != '') {
			$sql = $sql . ' where ' . $condition;
		}
		$result = $this->get_list ( $sql );
		$return = array ();
		foreach ( $result as $row ) {
			$member = new Member ();
			$member->account = $row ['Account'];
			$member->name = $row ['Name'];
			$member->dateOfBirth = $row ['DateOfBirth'];
			$member->gender = $row ['Gender'];
			$member->phoneNumber = $row ['PhoneNumber'];
			$member->email = $row ['Email'];
			$member->addr = $row ['Addr'];
			$member->pass = $row ['Pass'];
			$member->createAt = $row ['CreateAt'];
			$member->updateAt = $row ['UpdateAt'];
			$member->images = $row ['Images'];
			$member->auth = $row ['Auth'];
			$return [] = $member;
		}
		return $return;
	}
	public function __toString() {
		return $this->account . ' - ' . $this->name . ' - ' . $this->dateOfBirth . ' - ' . $this->gender . ' - ' . $this->phoneNumber . ' - ' . $this->email . ' - ' . $this->addr . ' - ' . $this->pass . ' - ' . $this->createAt . ' - ' . $this->updateAt . ' - ' . $this->images . ' - ' . $this->auth;
	}
}